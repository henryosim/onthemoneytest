<?php


namespace App\Tests\Command;

use PharIo\Manifest\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class PostcodesDownloadAndImportPostcodeCommandTest
 * @package App\Tests
 */
class PostcodesDownloadAndImportCommand extends KernelTestCase
{
    /** @test  */
    public function successful_execution_of_Command()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $command = $application->find('postcodes:download-import');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'downloadUrl' => 'testCommand'
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[OK]', $output);
    }
}
