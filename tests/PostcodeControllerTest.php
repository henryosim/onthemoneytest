<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class PostcodeControllerTest
 * @package App\Tests
 */
class PostcodeControllerTest extends WebTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $kernel = self::bootKernel();
        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->entityManager->close();
        $this->entityManager = null;
    }

    /** @test  */
    public function check_index_route()
    {
        $this->client->request('GET', '/');
        $responseCode = $this->client->getResponse()->getStatusCode();
        $this->assertResponseStatusCodeSame($responseCode, 200);
    }

    /** @test  */
    public function check_postcode_search_route()
    {
        $this->client->request('GET', '/postcode-search/LU61FY');
        $responseCode = $this->client->getResponse()->getStatusCode();
        $this->assertResponseStatusCodeSame($responseCode, 200);
    }

    /** @test  */
    public function check_postcode_location_route()
    {
        $this->client->request('GET', '/postcode-location/11.101474/-2.242851');
        $responseCode = $this->client->getResponse()->getStatusCode();
        $this->assertResponseStatusCodeSame($responseCode, 200);
    }
}
