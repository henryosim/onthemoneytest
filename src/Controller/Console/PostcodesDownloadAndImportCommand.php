<?php

namespace App\Controller\Console;

use App\Repository\PostcodeRepository;
use Doctrine\DBAL\Driver\Exception as DbalDriverException;
use Doctrine\DBAL\Exception as DbalException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class PostcodesDownloadAndImportCommand extends Command
{
    protected static $defaultName = 'postcodes:download-import';
    private $destinationDirectory = './public/postcodes/';
    private $subDirectory = 'Data/multi_csv/';
    private $maxFileSize = '3221223823'; // 3gb;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var int */
    private $numOfNewRecords;

    /** @var PostcodeRepository */
    private $postCodeRepository;

    /** @var int */
    private $numOfRemovedRecords;

    public function __construct(EntityManagerInterface $entityManager, PostcodeRepository $postCodeRepository)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->postCodeRepository = $postCodeRepository;
        $this->numOfNewRecords = 0;
        $this->numOfRemovedRecords = 0;
    }

    /**
     * Command configure method
     */
    protected function configure(): void
    {
        $this->setDescription('Download Stock data command')
            ->addArgument(
                'downloadUrl',
                InputArgument::OPTIONAL,
                'enter file download url',
                'https://parlvid.mysociety.org/os/ONSPD/2020-05.zip'
            );
    }

    /**
     * Command execute method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $remoteAddress = $input->getArgument('downloadUrl');
        $baseName = pathinfo($remoteAddress, PATHINFO_BASENAME);
        $io = new SymfonyStyle($input, $output);

        $io->writeln(date("Y-m-d h:i:sa") . " - Starting file download..");
        $this->copyRemoteFile($remoteAddress, $baseName);
        $io->writeln(date("Y-m-d h:i:sa") . " - Finished file download");

        $io->writeln(date("Y-m-d h:i:sa") . " - Starting clearing existing data..");
        $this->deleteExistingDataInDatabase();
        $io->writeln(date("Y-m-d h:i:sa") . " - Finished clearing existing data");

        $io->writeln(date("Y-m-d h:i:sa") . " - Starting data extraction..");
        $this->extractData($baseName, $this->subDirectory);
        $io->writeln(date("Y-m-d h:i:sa") . " - Finished data extraction");

        $io->writeln(date("Y-m-d h:i:sa") . " - Starting importing CSVs..");
        $this->processCsvFiles();
        $io->writeln(date("Y-m-d h:i:sa") . " - Finished importing CSVs");

        $io->success('Completed. ' . $this->numOfRemovedRecords . ' record(s) removed and ' . $this->numOfNewRecords . ' record(s) added');
        return Command::SUCCESS;
    }

    /**
     * Delete all existing records
     * @return void
     */
    private function deleteExistingDataInDatabase(): void
    {
        $numOfRemovedRecords = $this->postCodeRepository->deleteAllPostcodes();
        $this->numOfRemovedRecords = $numOfRemovedRecords;
        echo $numOfRemovedRecords . ' records removed from the database.';
    }

    /**
     * Process csv files, write content to database
     * @throws DbalException
     */
    private function processCsvFiles(): void
    {
        $filesystem = new Filesystem();
        $directory = $this->destinationDirectory . $this->subDirectory;
        if (!$filesystem->exists($directory)) {
            throw new FileException(sprintf('could not find directory  "%s"', $directory));
        }

        $finder = new Finder();
        $finder->files()->in($directory);
        foreach ($finder as $file) {
            echo 'processing file :' . $file->getFilename() . PHP_EOL;
            $this->processFileIntoDatabase($file);
        }
    }

    /**
     * Process csv file and load into the database
     * @param SplFileInfo $file
     * @throws DbalException
     */
    private function processFileIntoDatabase(SplFileInfo $file): void
    {
        $csvFilename = $file->getPathname();
        $txtFilename = $this->destinationDirectory . 'Data/postcodes.txt';
        $this->addCsvToTextFile($csvFilename, $txtFilename);
        try {
            $this->customLoadTxtIntoDatabase($txtFilename);
        } catch (DbalDriverException $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Add CSV to text file
     * @param string $csvFilename
     * @param string $txtFilename
     * @return void
     */
    private function addCsvToTextFile(string $csvFilename, string $txtFilename): void
    {
        // Read CSV file
        $handle = fopen($csvFilename, "r");
        $raw_string = fgets($handle);
        $header = str_getcsv($raw_string);

        file_put_contents($txtFilename, "");

        $textContent = "";
        $dateTime = (new \DateTimeImmutable())->format('Y-m-d H:i:s');

        // Iterate over every line of the file
        while (($raw_string = fgets($handle)) !== false) {
            $row = array_combine($header, str_getcsv($raw_string));
            $row['pcd_min'] = str_replace(' ', '', $row['pcd']);
            $textContent .= $row['pcd_min'] . ";" . $row['lat'] . ";" . $row['long'] . ";" . $dateTime . ";" . $dateTime . "\n";
            $this->numOfNewRecords++;
        }

        file_put_contents($txtFilename, $textContent);
        fclose($handle);
    }

    /**
     * SQL query to load txt file;
     * @param string $txtRealPath
     * @throws DbalDriverException
     * @throws DbalException
     */
    private function customLoadTxtIntoDatabase(string $txtRealPath): void
    {
        $FIELDS = "( postcode, latitude, longitude, created_at, updated_at)";
        $sql = "LOAD DATA LOCAL INFILE '$txtRealPath' INTO TABLE  postcode FIELDS TERMINATED BY ';' $FIELDS;";
        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $stmt->execute();
    }

    /**
     * Copy file from remote address
     * @param string $remoteAddress
     * @param string $baseName
     * @return void
     * @throws Exception
     */
    private function copyRemoteFile(string $remoteAddress, string $baseName): void
    {
        $filesystem = new Filesystem();
        $filesystem->copy($remoteAddress, $this->destinationDirectory . $baseName);
    }

    /**
     * Open archive and extract data directory
     * @param string $baseName
     * @param string $subDirectory
     * @return void
     * @throws Exception
     */
    private function extractData(string $baseName, string $subDirectory = ''): void
    {
        $filesystem = new Filesystem();
        $file = $this->destinationDirectory . $baseName;
        if (!$filesystem->exists($file)) {
            throw new FileNotFoundException(sprintf('File "%s" could not be found.', $baseName));
        }

        $zip = new \ZipArchive();
        $res = $zip->open($this->destinationDirectory . $baseName);
        if ($res === true) {
            if (!$zip->locateName($subDirectory)) {
                throw new Exception(sprintf('sub directory "%s" could not be found in archive.', $subDirectory));
            }

            $zipEntries = [];
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $stats = $zip->statIndex($i);

                if (!str_starts_with($stats['name'], $subDirectory)) {
                    continue;
                }

                if ($stats['size'] > $this->maxFileSize) {
                    throw new Exception(sprintf('"%s" File too large, decompression denied.', $stats['name']));
                }

                $zipEntries[] = $stats['name'];
            }
            $zip->extractTo($this->destinationDirectory, $zipEntries);
            $zip->close();
        } else {
            throw new Exception(sprintf('Unable to open zip file "%s"', $baseName));
        }
    }
}
