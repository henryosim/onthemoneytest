<?php

namespace App\Controller;

use App\Repository\PostcodeRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class PostcodeController
 * @package App\Controller
 */
class PostcodeController extends AbstractController
{
    /**
     * @var PostcodeRepository
     */
    private $postCodeRepository;

    /**
     * PostcodeController constructor.
     * @param PostcodeRepository $postCodeRepository
     */
    public function __construct(PostcodeRepository $postCodeRepository)
    {
        $this->postCodeRepository = $postCodeRepository;
    }

    /**
     * @Route(path="/", name="home")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to Postcode Index!',
            'path' => 'src/Controller/PostcodeController.php',
            'routes' => [
                'postcode-search/{partial}' => 'return postcodes with partial string matches',
                'postcode-location/{latitude}/{longitude}' => 'return postcodes near a specified location (latitude / longitude)',
            ]
        ]);
    }

    /**
     * @Route(path="postcode-search/{partial}", name="postcode-search")
     *
     * @param string $partial
     * @return Response
     */
    public function search(string $partial): Response
    {
        if (strlen($partial) < 2) {
            return $this->json(['error' => 'keyword must be longer than 2 characters']);
        }

        $postcodes = $this->postCodeRepository->findByPartialPostcodeMatch($partial);
        return $this->json(['postcodes' => $postcodes]);
    }

    /**
     * @Route(path="postcode-location/{latitude}/{longitude}", name="postcode-ocation")
     *
     * @param float $latitude
     * @param float $longitude
     * @return Response
     */
    public function location(float $latitude, float $longitude): Response
    {
        $postcodes = $this->postCodeRepository->findBy(['latitude' => $latitude, 'longitude' => $longitude]);
        return $this->json(['postcodes' => $postcodes]);
    }
}
