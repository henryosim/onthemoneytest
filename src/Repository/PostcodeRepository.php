<?php

namespace App\Repository;

use App\Entity\Postcode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\This;

/**
 * @method Postcode|null find($id, $lockMode = null, $lockVersion = null)
 * @method Postcode|null findOneBy(array $criteria, array $orderBy = null)
 * @method Postcode[]    findAll()
 * @method Postcode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostcodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Postcode::class);
    }

    /**
     * Find by partial postcode match. input must be at least 3 characters long
     * @param string $value
     * @return Postcode[] Returns an array of Postcode objects
     */
    public function findByPartialPostcodeMatch(string  $value): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.postcode like :val')
            ->setParameter('val', '%'.$value.'%')
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Delete all postcodes
     * @return int
     */
    public function deleteAllPostcodes(): int
    {
        return $this->createQueryBuilder('p')->delete()->getQuery()->execute();
    }
}
